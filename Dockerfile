FROM openjdk:11
VOLUME /tmp
ARG JAR_FILE=target/summarization-0.0.1.war
COPY ${JAR_FILE} app.war
ENTRYPOINT ["java", "-jar", "/app.war"]
