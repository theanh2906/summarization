<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Title</title>
<%@ include file="../commons/css_import.jsp"%>
<link rel="stylesheet" type="text/css" href="assets/css/login-register.css"/>
</head>
<body>
	<div id="logreg-forms">
		<form:form class="form-signin" method="post">
			<h1 class="h3 mb-3 font-weight-normal" style="text-align: center">
				Sign in</h1>
			<div class="social-login">
				<button class="btn facebook-btn social-btn" type="button">
					<span><em class="fab fa-facebook-f"></em> Sign in with
						Facebook</span>
				</button>
				<button class="btn google-btn social-btn" type="button">
					<span><em class="fab fa-google-plus-g"></em> Sign in with
						Google+</span>
				</button>
			</div>
			<p style="text-align: center">OR</p>
			<div class="warn mb-2">
				<c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION}">
					<c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}" />
				</c:if>
			</div>
			<input type="text" id="inputEmail" class="form-control" placeholder="Username" required="" autofocus="" name="username">
			<input type="password" id="inputPassword" class="form-control" placeholder="Password" required="" name="password">

			<button class="btn btn-success btn-block" type="submit">
				<em class="fas fa-sign-in-alt"></em> Sign in
			</button>
			<a href="#" id="forgot_pswd">Forgot password?</a>
			<hr>
			<button class="btn btn-primary btn-block" type="button"
				id="btn-signup">
				<em class="fas fa-user-plus"></em> Sign up New Account
			</button>
		</form:form>

		<form action="/reset/password/" class="form-reset">
			<input type="email" id="resetEmail" class="form-control"
				placeholder="Email address" required="" autofocus="">
			<button class="btn btn-primary btn-block" type="submit">Reset
				Password</button>
			<a href="#" id="cancel_reset"><em class="fas fa-angle-left"></em>
				Back</a>
		</form>

		<%-- <%@ include file="register.jsp" %> --%>
		<br>

	</div>
	<%@ include file="../commons/js_import.jsp"%>
	<script type="text/javascript">
/* 	function toggleResetPswd(e){
	    e.preventDefault();
	    $('#logreg-forms .form-signin').toggle() // display:block or none
	    $('#logreg-forms .form-reset').toggle() // display:block or none
	} */

    $('#btn-signup').on("click", function () {
		window.location.href="/summarization/register"
	}); // display:block or none

/* 	$(()=>{
	    // Login Register Form
	    $('#logreg-forms #forgot_pswd').click(toggleResetPswd);
	    $('#logreg-forms #cancel_reset').click(toggleResetPswd);
 	    $('#logreg-forms #btn-signup').click(toggleSignUp); 
	    $('#logreg-forms #cancel_signup').click(toggleSignUp);
	}) */
	</script>
</body>
</html>
