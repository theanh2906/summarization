<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="icon" href="/docs/4.1/assets/img/favicons/favicon.ico">
<link
	href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet">
<title>${headerTitle}</title>

<!-- Bootstrap core CSS -->
<%@include file="commons/css_import.jsp"%>
</head>

<body>
	<c:if test="${not empty status }">
		<div class="alert alert-${status}">${message}</div>
	</c:if>
	<main role="main" class="container">
		<%@ include file="components/header.jsp"%>
		<div>
			<div class="blog-main">
				<div class="blog-post mt-5">
					<h2 class="blog-post-title">Create new post</h2>
					<hr>
				</div>
				<form:form class="col-md-12 col-lg-12" id="create-post"
					action="/summarization/create-post" method="post" modelAttribute="input">
					<form:errors path="title" cssClass="warn"/>
					<form:input type="text" class="form-control" id="title" name="title" path="title" placeholder="Title"/> 
					<form:input type="text" class="form-control" id="author" name="author" path="author" placeholder="Author"/> 
					<form:select class="form-control" id="categoryId" name="categoryId" path="categoryId" placeholder="Category">
						<option value="" selected data-default>--Select category--</option>
						<c:forEach items="${listCategories}" var="cat">
							<option value="${cat.categoryId}">${cat.categoryName}</option>
						</c:forEach>
					</form:select>
					<small id="recommendCategory"></small>
					<div class="form-group">
						<form:errors path="abstracts" cssClass="warn"/>
						<form:textarea class="form-control" id="abstracts" rows="5" name="abstracts" path="abstracts" placeholder="Abstract would be placed here"></form:textarea>
						<br><button id="generate-abstract" class="btn btn-primary mt-2">Generate abstract</button>
<%--						<br><button id="recommendCategory" class="btn btn-primary mt-2">Recommend category</button>--%>
					</div>
					<form:errors path="content" cssClass="warn"/>
					<form:textarea class="form-control" id="content" rows="15" name="content" path="content" placeholder="Content of post would be placed here"></form:textarea>
					<div class="form-group mt-2">
						<form:button type="submit" class="btn btn-primary">Submit</form:button>
						<button id="generate-text" class="btn btn-primary">Random</button>
						<button type="reset" class="btn btn-primary" id="reset">Reset</button>
					</div>
					<div id="result"></div>
				</form:form>
			</div>
		</div>
	</main>
	<%@ include file="commons/js_import.jsp"%>
	<script type="text/javascript">
		$("#create-post").validate({
			errorClass: "warn",
			rules: {
				title: "required",
				content: "required",
				category: "required",
				abstracts: "required"
			},
			messages: {
				title: "Title should not be null",
				content: "Content should not be null",
				category: "Please select category",
				abstracts: "Abstract should not be null"
			}
		})
	</script>
	<script src="/summarization/assets/js/ckeditor-init.js"></script>
	<script src="/summarization/assets/js/common.js"></script>
</body>
</html>
