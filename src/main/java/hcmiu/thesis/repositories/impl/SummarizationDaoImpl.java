package hcmiu.thesis.repositories.impl;

import java.io.IOException;

import org.springframework.stereotype.Repository;

import hcmiu.thesis.repositories.SummarizationDao;
import lombok.NoArgsConstructor;

@Repository
@NoArgsConstructor
public class SummarizationDaoImpl implements SummarizationDao {

	@Override
	public String getSummary(String text, int sentenceNum)  {
		try {
			return Summarizer.summarize(text,sentenceNum);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String getLSA(String text, int number) {
		try {
			return LSA.summarize(text, number);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
