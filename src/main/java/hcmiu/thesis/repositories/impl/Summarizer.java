package hcmiu.thesis.repositories.impl;

import hcmiu.thesis.utils.WordUtil;
import opennlp.tools.util.StringList;
import org.aspectj.util.FileUtil;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Summarizer {

	public static String summarize(String text, int maxSummarySize) throws IOException {
        NGram ngram = new NGram();
        StringList words = ngram.tokenize(text);
        ngram.generate(words, 1);
        ngram.filterStopWords();
        ngram.sort();
        ngram.getSentenceUsingModel(text);
        String firstSentence = ngram.getSentences()[0];
        List<String> setSummarySentences = new ArrayList<>();
        setSummarySentences.add(firstSentence);
        // Freak string in the sorted list
        for (int i = 0; i <= ngram.getSorted().size(); i++)
        {
            String firstMatchingSentence = ngram.search(ngram.getSorted().get(i));
            if (!setSummarySentences.contains(firstMatchingSentence)){
                // add to summary list
            	setSummarySentences.add(firstMatchingSentence);
            }

            if (setSummarySentences.size() == maxSummarySize) {
                break;
            }
        }

        // construct the summary size out of select sentences
        String summary = "";
		// Freak string sentence in sentences
        for (String sentence : ngram.getSentences())
        {
            if (setSummarySentences.contains(sentence)) {
                // produce each sentence with a bullet point and good amounts of spacing
                summary = summary.concat(" ").concat(sentence).trim();
            }
        }
        return summary;
    }

    public static List<String> getCategory(String text) throws IOException{
        NGram ngram = new NGram();
        StringList words = ngram.tokenize(text);
        ngram.generate(words, 1);
        ngram.filterStopWords();
        ngram.sort();
        String concatedString = WordUtil.concatList(ngram.getSorted());
        return POSTagger.nouns(concatedString).subList(0, 5);
    }

    public static void main(String[] args) throws IOException {
	    String text = FileUtil.readAsString(ResourceUtils.getFile("classpath:test.txt"));
        getCategory(text).forEach(s -> System.out.println(s));
    }
}