package hcmiu.thesis.repositories;

import hcmiu.thesis.models.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends CrudRepository<Post, Integer>, JpaRepository<Post, Integer> {
	Post findByTitleAndUserId(String title, Integer userId);

	List<Post> findByTitleOrContentOrAuthorContainsIgnoreCase(String title, String content, String author);
}
