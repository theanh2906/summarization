package hcmiu.thesis.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import hcmiu.thesis.models.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>, CrudRepository<User, Integer>{
	
	User findByEmail(String email);
	
	User findByAccountId(Integer accountId);
}
