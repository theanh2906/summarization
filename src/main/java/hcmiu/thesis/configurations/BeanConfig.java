package hcmiu.thesis.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import hcmiu.thesis.repositories.SummarizationDao;
import hcmiu.thesis.repositories.impl.SummarizationDaoImpl;

@Configuration
public class BeanConfig {
	
	@Bean
	public SummarizationDao summarizationDao () {
		return new SummarizationDaoImpl();
	}
}
