package hcmiu.thesis.utils;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class WebScaping {
	
	public static String scaping(String url) {
		String content = "";
		try {
			Document doc = Jsoup.connect(url)
					.userAgent("client")
					.timeout(20000)
					.get();
			Elements list = doc.select("p");
			for (Element element : list) {
				content = (content + "<p>" + element.text()).trim() + "</p>";		
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return content;
	}
}