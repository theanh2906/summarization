package hcmiu.thesis.utils;

import org.springframework.beans.BeanUtils;

import hcmiu.thesis.dtos.RegisterInfo;
import hcmiu.thesis.models.Account;

public class Mapper {
	public static Account toModel (RegisterInfo dto) {
		Account model = new Account();
		BeanUtils.copyProperties(dto, model);
		return model;
	}
	
	public static RegisterInfo toDto (Account model) {
		RegisterInfo dto = new RegisterInfo();
		BeanUtils.copyProperties(model, dto);
		return dto;
	}
	
	
}
