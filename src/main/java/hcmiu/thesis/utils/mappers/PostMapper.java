package hcmiu.thesis.utils.mappers;

import hcmiu.thesis.dtos.PostDTO;
import org.springframework.beans.BeanUtils;

import hcmiu.thesis.dtos.CreatePostDTO;
import hcmiu.thesis.models.Post;

public class PostMapper {
	public static Post toModel(CreatePostDTO dto) {
		Post model = Post.getIntance();
		BeanUtils.copyProperties(dto, model);
		return model;
	}

	public static PostDTO toDto(Post model) {
		PostDTO dto = new PostDTO();
		BeanUtils.copyProperties(model, dto);
		return dto;
	}
}
