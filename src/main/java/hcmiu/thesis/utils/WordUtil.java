package hcmiu.thesis.utils;

import opennlp.tools.tokenize.SimpleTokenizer;
import opennlp.tools.util.StringList;

import java.util.List;

public class WordUtil {
	public static int countWords (String text) {
		int total = 0;
		text = text.replaceAll("\\p{Punct}", "");
		StringList list = new StringList(SimpleTokenizer.INSTANCE.tokenize(text));
		total = list.size();
		return total;
	}

	public static String concatList(List<String> listWords) {
		return String.join(" ", listWords)
			.replaceAll(",","");
	}
	
}
