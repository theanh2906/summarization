package hcmiu.thesis.utils;

import hcmiu.thesis.commons.EmailTypes;
import hcmiu.thesis.dtos.RegisterInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:message.properties")
public class UrlUtil {

	@Value("${base-root}")
	private static String HOME_ROOT;
	@SuppressWarnings("null")
	public static String createLink (EmailTypes emailTypes, RegisterInfo info) {
		String link = null;
		
		switch (emailTypes) {
		case SUCCESSFUL_REGISTRATION:
			link = HOME_ROOT + "/confirmed?username=" + info.getUsername();

		default:
			break;
		}
		
		return link;
	}
}
