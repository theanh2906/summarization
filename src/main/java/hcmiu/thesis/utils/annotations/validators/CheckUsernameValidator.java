package hcmiu.thesis.utils.annotations.validators;

import java.util.Optional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import hcmiu.thesis.models.Account;
import hcmiu.thesis.repositories.AccountReposity;

public class CheckUsernameValidator implements ConstraintValidator<CheckUsername, String> {

	@Autowired
	AccountReposity repo;
	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		// TODO Auto-generated method stub
		if (value.isBlank()) {
			return true;
		} else {
		Optional<Account> account = Optional.ofNullable(repo.findByUsername(value));
		return account.isEmpty() ? true : false;
		}
	}

}
