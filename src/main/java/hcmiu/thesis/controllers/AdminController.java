package hcmiu.thesis.controllers;

import hcmiu.thesis.dtos.EditAccountDTO;
import hcmiu.thesis.models.Account;
import hcmiu.thesis.services.AdminService;
import hcmiu.thesis.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Comparator;
import java.util.stream.Collectors;

@Controller
@RequestMapping ("/admin")
public class AdminController {
	
	@Autowired
	private AdminService adminService;

	@Autowired
	private PostService postService;
	
	@GetMapping({"/home", "", "/", "/account-management"})
	public ModelAndView viewAdmin() {
		ModelAndView mav = new ModelAndView("admin/home");
		mav.addObject("allAccounts",
			adminService.getAllAccounts()
					.stream()
					.sorted(Comparator.comparing(Account::getAccountId))
					.collect(Collectors.toList()));
		mav.addObject("accounts", new EditAccountDTO());
		mav.addObject("headerTitle", "Admin Console");
		return mav;
	}

	@GetMapping("/post-management")
	public ModelAndView postManagement() {
		ModelAndView mav = new ModelAndView("admin/post-management");
		mav.addObject("allPosts",
			postService.getAllPosts()
					.stream()
					.sorted((a, b) -> b.getCreateTime().compareTo(a.getCreateTime()))
					.collect(Collectors.toList()));
		mav.addObject("headerTitle", "Posts Management");
		return mav;
	}
	
	@GetMapping("/accounts/{id}/status/{isActive}")
	public String changeAccountStatus(@PathVariable Integer id, @PathVariable Boolean isActive) {
		adminService.changeAccountStatus(id, isActive);
		return "redirect:/admin/home";
	}

	@GetMapping("/posts/{id}/status/{isActive}")
	public String changePostStatus(@PathVariable Integer id, @PathVariable Boolean isActive) {
		adminService.changePostStatus(id, isActive);
		return "redirect:/admin/post-management";
	}
}
