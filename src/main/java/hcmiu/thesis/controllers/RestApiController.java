package hcmiu.thesis.controllers;

import hcmiu.thesis.dtos.PostDTO;
import hcmiu.thesis.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/")
public class RestApiController {
	
	@Autowired
	private PostService postService;
	
	@GetMapping("/categories")
	public List<String> allCategories () {
		return postService.getAllPosts().stream().map(PostDTO::getCategoryName).collect(Collectors.toList());
	}
}
