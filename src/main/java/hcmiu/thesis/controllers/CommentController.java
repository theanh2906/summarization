package hcmiu.thesis.controllers;

import hcmiu.thesis.dtos.CommentDTO;
import hcmiu.thesis.services.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDateTime;

@Controller
public class CommentController {

    @Autowired
    private CommentService commentService;

    @PostMapping("/comments")
    public @ResponseBody Boolean createComment(@RequestBody CommentDTO body) {
        body.setCreateTime(LocalDateTime.now());
        return commentService.createComment(body);
    }
}
