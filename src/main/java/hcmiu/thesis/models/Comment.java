package hcmiu.thesis.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@Table(name = "TBL_COMMENT")
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Comment {
	private static Comment INSTANCE;
	@Id	@GeneratedValue(strategy = GenerationType.IDENTITY)	@Column	private Integer commentId;
	@Column	private Integer userId;
	@Column	private Integer postId;
	@Column	private String content;
	@Column	private LocalDateTime createTime;
	@Column	private Integer status;
	@Column	private LocalDateTime updateTime;
	@Column	private String type;
	@Column (name = "reply_to_comment") private Integer replyTo;
	public static Comment getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new Comment();
		}
		return INSTANCE;
	}
}
