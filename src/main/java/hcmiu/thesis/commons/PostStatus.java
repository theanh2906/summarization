package hcmiu.thesis.commons;

public enum PostStatus {
	ACTIVE(1),
	INACTIVE(2),
	OUTDATED(3),
	DRAFT(4)	;

	Integer value;

	PostStatus(int value) {
		this.value = value;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}
}
