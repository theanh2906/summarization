package hcmiu.thesis.commons;

public enum UserRole {
    ADMIN("admin"),
    USER("user");

    String value;

    UserRole(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
