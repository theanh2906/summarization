package hcmiu.thesis.commons;

public class UrlConstant {
	public final static  String HOME = "/home";
	public final static String ADMIN = "/admin/**";
	public final static String POST = "/post";
	public static final String LOGIN = "/login";
	public static final String PROFILE = "/profile";
}
