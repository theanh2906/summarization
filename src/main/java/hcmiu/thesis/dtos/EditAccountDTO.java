package hcmiu.thesis.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class EditAccountDTO {
	private Integer id;
	private String isActive;
	private String isDisabled;
}
