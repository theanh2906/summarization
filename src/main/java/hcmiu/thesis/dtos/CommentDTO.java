package hcmiu.thesis.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.ALWAYS)
public class CommentDTO {
	
	private static CommentDTO INSTANCE;
	
	private Integer commentId;
	
	private Integer postId;

	private String title;
	
	private String username;
	
	private String avatarPath;
	
	private String content;
	
	private String type;
	
	private LocalDateTime createTime;
	
	private Integer replyTo;
	
	private List<ReplyDTO> replies;

	public CommentDTO(Integer commentId, Integer postId, String username, String avatarPath, String content, String type,
			LocalDateTime createTime, Integer replyTo, String title) {
		super();
		this.commentId = commentId;
		this.postId = postId;
		this.username = username;
		this.avatarPath = avatarPath;
		this.content = content;
		this.type = type;
		this.createTime = createTime;
		this.replyTo = replyTo;
		this.title = title;
	}
	public static CommentDTO getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new CommentDTO();
		}
		return INSTANCE;
	}

	@Override
	public String toString() {
		return "CommentDTO{" + ",\n" +
				" commentId=" + commentId + ",\n" +
				" postId=" + postId + ",\n" +
				" title=" + title + ",\n" +
				" username=" + username + ",\n" +
				" avatarPath=" + avatarPath + ",\n" +
				" content=" + content + ",\n" +
				" type=" + type + ",\n" +
				" createTime=" + createTime + ",\n" +
				" replyTo=" + replyTo + ",\n" +
				" replies=" + replies + ",\n" +
				"}";
	}
}
