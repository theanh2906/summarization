package hcmiu.thesis.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor
@NoArgsConstructor
public class DemoDTO {
    private String textInput;
    private Integer sentenceNum;
    private String method;
}
