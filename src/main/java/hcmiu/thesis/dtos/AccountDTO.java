package hcmiu.thesis.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccountDTO {
	
	@NotBlank
	@Size (min = 8, max = 20, message = "Username you typed is too long. It should be between 8 and 20")
	private String username;
	
	@NotBlank
	@Size (min = 8, max = 20, message = "Password you typed is too long. It should be between 8 and 20")
	private String password;
	
}
