package hcmiu.thesis.dtos;

import java.time.LocalDateTime;

import javax.validation.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CreatePostDTO {
	
	@NotEmpty(message = "Title should not be empty")
	private String title;
	private Integer userId;
	private String abstracts;
	private Integer categoryId;
	private String author;
	@NotEmpty(message = "Content should not be empty")
	private String content;
	private LocalDateTime createTime;
}
