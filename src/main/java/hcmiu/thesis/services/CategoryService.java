package hcmiu.thesis.services;

import java.util.List;

import hcmiu.thesis.dtos.CategoryDTO;

public interface CategoryService {
	List<CategoryDTO> getAllCategories();
}
