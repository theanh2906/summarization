package hcmiu.thesis.services;

import hcmiu.thesis.models.User;

public interface UserService {
	User findById(Integer userId);
}

