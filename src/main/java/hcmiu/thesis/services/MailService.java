package hcmiu.thesis.services;

import hcmiu.thesis.commons.EmailTypes;
import hcmiu.thesis.dtos.RegisterInfo;

public interface MailService {
	void sendMail(RegisterInfo info, EmailTypes emailType);
}
