package hcmiu.thesis.services;

import hcmiu.thesis.models.Account;
import hcmiu.thesis.models.Post;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface AdminService {
	
	List<Account> getAllAccounts();
	
	void deleteAccount(Integer id);
	
	Account changeAccountStatus(Integer id, Boolean isActive);
	
	Account changeAccountStatus(String username, Boolean isActive);

	Post changePostStatus(Integer id, Boolean isActive);
}
