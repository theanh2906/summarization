package hcmiu.thesis.services;

import hcmiu.thesis.dtos.RegisterInfo;

public interface AuthenticationService {
	/**
	 * Check existence of user 
	 * @param String username
	 * @return true if exists/ false if not exists
	 */
	boolean checkExistAccount(String username);
	
	String register (RegisterInfo info);
	
	Integer findUserIdByUsername(String username);
	
}
