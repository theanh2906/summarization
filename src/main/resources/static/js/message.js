export const SUCCESS_COMMENT = "Your comment was sent. Please wait for approval";
export const ERROR = "Something went wrong! Please try again";
export const ROOT_URL = "http://localhost:8080/summarization/";

